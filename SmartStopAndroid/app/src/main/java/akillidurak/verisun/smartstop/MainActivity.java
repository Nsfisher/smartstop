package akillidurak.verisun.smartstop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;


import org.json.JSONObject;

import akillidurak.verisun.smartstops.core.Service;
import akillidurak.verisun.smartstops.model.BusLines.GetStopLines;
import akillidurak.verisun.smartstops.model.BusStopLocation.BusStopLocationInfo;
import akillidurak.verisun.smartstops.model.BusStopMapping.BusStopInfo;
import akillidurak.verisun.smartstops.model.Foursquare.FoursData;
import akillidurak.verisun.smartstops.model.GetBusline.GetBuslineInfo;
import akillidurak.verisun.smartstops.model.HowToGo.How2Go;
import akillidurak.verisun.smartstops.model.HowToGo.Sentence;
import akillidurak.verisun.smartstops.model.LinePoints.LinePoints;
import akillidurak.verisun.smartstops.model.LineStops.GetLineStops;
import akillidurak.verisun.smartstops.model.SearchMap.Search;
import akillidurak.verisun.smartstops.model.TimeTable.TimeTableModel;
import akillidurak.verisun.smartstops.model.VehicleLocation.LineInfo;
import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;
import akillidurak.verisun.smartstops.model.VehicleLocation.NearestStop;
import akillidurak.verisun.smartstops.model.VehicleLocation.VehicleInfo;
import akillidurak.verisun.smartstops.model.VehicleLocation.VehicleLocationInfo;


public class MainActivity extends AppCompatActivity {

    private VehicleLocationInfo vehicleLocationInfo;
    private VehicleInfo vehicleInfo;
    private LineInfo lineInfo;
    private Locations locations;
    private NearestStop nearestStop;
    public MainActivity instance;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;


        Service.service.getSearch("merkez","3",1,1,1,new Service.BaseCallBack<Search>() {
            @Override
            public void onSuccessResponse(Search search) {

                Log.e("SearchInfo",""+search);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });
        //https://verisun.com:8543/ekentankara/rest/buslines/getVehicleLocationInfo?topLeftLatitude=39.937265&topLeftLongitude=32.840270&topRightLatitude=39.937332&topRightLongitude=32.899658&bottomLeftLatitude=39.910486&bottomLeftLongitude=32.843144&bottomRightLatitude=39.910085&bottomRightLongitude=32.899049&cityId=3
        //https://verisun.com:8543/ekentankara/rest/buslines/getVehicleLocationInfo?topLeftLatitude=39.937265&topLeftLongitude=32.840270&topRightLatitude=39.937332&topRightLongitude=32.899658&bottomLeftLatitude=39.910486&bottomLeftLongitude=32.843144&bottomRightLatitude=39.910085&bottomRightLongitude=32.899049&cityId=3

        /*
        Service.service.getVehicleLocationInfo("39.937265","32.840270","39.937332","32.899658","39.910486","32.843144","39.910085","32.899049","3",new Service.BaseCallBack<VehicleLocationInfo>() {
                    @Override
                    public void onSuccessResponse(VehicleLocationInfo vehicleLocationInfo) {
                        Log.e("VehicleInfo",""+vehicleLocationInfo);
                    }
                    @Override
                    public void onError(String errorMessage) {
                        Log.e("", "");

                    }
                });





         // https://verisun.com:8543/ekentankara/rest/busstop/getBusStopLocationInfo?topLeftLatitude=39.937265&topLeftLongitude=32.840270&topRightLatitude=39.937332&topRightLongitude=32.899658&
        // bottomLeftLatitude=39.910486&bottomLeftLongitude=32.843144&bottomRightLatitude=39.910085&bottomRightLongitude=32.899049&cityId=3

        Service.service.getBusStopLocationInfo("39.937265","32.840270","39.937332","32.899658","39.910486","32.843144","39.910085","32.899049","3", new Service.BaseCallBack<BusStopLocationInfo>() {
            @Override
            public void onSuccessResponse(BusStopLocationInfo busStopLocationInfo) {
            Log.e("busStopLocationInfo",""+busStopLocationInfo);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });
        Service.foursquareService.getFoursquare(40.7,-74.0,"sushi", new Service.BaseCallBack<FoursData>() {
            @Override
            public void onSuccessResponse(FoursData foursData) {
                Log.e("FoursData:",""+foursData);
            }
            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });


        Service.service.getHowtoGoDetail("39.949","32.788","40.124473","32.991726","","bus","1",new Service.BaseCallBack<How2Go>(){

            @Override
            public void onSuccessResponse(How2Go how2Go) {
              Log.e("How2Go",""+how2Go);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });

        Service.service.getTimeTable( "106","3", new Service.BaseCallBack<TimeTableModel>() {
            @Override
            public void onSuccessResponse(TimeTableModel timeTable) {
                Log.e("TimeTableResponse:", "" + timeTable);
            }
            @Override
            public void onError(String errorMessage) {
                Log.e("TimeTableError:", "" + errorMessage);
            }
        });

        Service.service.getBusLine(125537, new Service.BaseCallBack<GetBuslineInfo>() {
            @Override
            public void onSuccessResponse(GetBuslineInfo getBuslineInfo) {
                Log.e("Succes", "" + getBuslineInfo);
            }
            @Override
            public void onError(String errorMessage) {
                Log.e("Error", " " + errorMessage);
            }
        });

        Service.service.getLineStop(125538, new Service.BaseCallBack<GetLineStops>() {
            @Override
            public void onSuccessResponse(GetLineStops getLineStops) {
                Log.e("getLineStop", "" + getLineStops);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("getLineStopError", "" + errorMessage);
            }
        });

        Service.service.getStopLines(50655, new Service.BaseCallBack<GetStopLines>() {
            @Override
            public void onSuccessResponse(GetStopLines getStopLines) {
                Log.e("GetStopLines", "" + getStopLines);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("getStopLines", "" + errorMessage);
            }
        });



        Service.service.getBusStop(56392,"3", new Service.BaseCallBack<BusStopInfo>() {
            @Override
            public void onSuccessResponse(BusStopInfo busStopInfo) {
                Log.e("BusStopInfo",""+busStopInfo);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });

        Service.service.getLinePoints(125537, new Service.BaseCallBack<LinePoints>() {
            @Override
            public void onSuccessResponse(LinePoints linePoints) {
                Log.e("", "");
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("", "");
            }
        });*/
        Service.service.getHowToGoSentence(39.949, 32.788, 40.124473,32.991726,"tr", new Service.BaseCallBack<Sentence>() {
            @Override
            public void onSuccessResponse(Sentence sentence) {
                Log.e("Sentence",""+sentence);
            }

            @Override
            public void onError(String errorMessage) {
                Log.e("","");
            }
        });
    }





}
