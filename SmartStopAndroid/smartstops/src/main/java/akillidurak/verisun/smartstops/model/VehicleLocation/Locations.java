package akillidurak.verisun.smartstops.model.VehicleLocation;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 25/05/16.
 */
public class Locations   {

        @SerializedName("latitude")
        Double latitude;

        @SerializedName("longitude")
        Double longitude;

        public Double getLatitude() {
            return latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }


    @Override
    public String toString() {

        return "locations {" +
                super.toString() + '\'' +
                "latitude"    + latitude  + '\'' +
                "longitude"   + longitude + '\'' + "}"
                ;
    }
}
