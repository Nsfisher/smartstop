package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/***
 *** Created by nsfisher on 24/06/16.
 ***/


public class How2Go  {
    /*
    {
        "how2go": [
        {
            "stats": {
            "points": 4710,
                    "duration": 47,
                    "hops": 1
        },
            "vehicles": {
            "bus": 1,
                    "metro": 0,
                    "tram": 0,
                    "ferry": 0,
                    "ido": 0,
                    "dentur": 0,
                    "turyol": 0,
                    "walk": 0
        },
            "path": [
            {
                "name": "İŞKUR ÜMRANİYE",
                    "time": "17:31",
                    "duration": 32,
                    "code": "A2339A",
                    "type": "bus",
                    "action": "stop",
                    "lat": 41.02354049682617,
                    "lng": 29.111520767211914
            },
            {
                "nodes": [
                {
                    "lat": 41.02354049682617,
                        "lng": 29.111520767211914
                },
                {
                    "lat": 41.02471160888672,
                        "lng": 29.104509353637695
                },
                {
                    "lat": 41.02484130859375,
                        "lng": 29.1011905670166
                },
                {
                    "lat": 41.025611877441406,
                        "lng": 29.09503936767578
                },
                {
                    "lat": 41.025169372558594,
                        "lng": 29.09126091003418
                },
                {
                    "lat": 41.02457046508789,
                        "lng": 29.08551025390625
                },
                {
                    "lat": 41.02349853515625,
                        "lng": 29.080690383911133
                },
                {
                    "lat": 41.02281951904297,
                        "lng": 29.074970245361328
                },
                {
                    "lat": 41.022621154785156,
                        "lng": 29.070940017700195
                },
                {
                    "lat": 41.02199935913086,
                        "lng": 29.066669464111328
                },
                {
                    "lat": 41.022071838378906,
                        "lng": 29.062469482421875
                },
                {
                    "lat": 41.023799896240234,
                        "lng": 29.053449630737305
                },
                {
                    "lat": 41.03681945800781,
                        "lng": 29.043420791625977
                },
                {
                    "lat": 41.05120849609375,
                        "lng": 29.008529663085938
                },
                {
                    "lat": 41.047969818115234,
                        "lng": 29.007890701293945
                },
                {
                    "lat": 41.041648864746094,
                        "lng": 29.00394058227539
                }
                ],
                "duration": 15,
                    "type": "bus",
                    "line": "103",
                    "action": "go"
            },
            {
                "name": "AKARETLER",
                    "time": "18:18",
                    "duration": 47,
                    "code": "L0002B",
                    "type": "bus",
                    "action": "stop",
                    "lat": 41.041648864746094,
                    "lng": 29.00394058227539
            }
            ],
            "count": 3
        }
        ],
        "processInfo": 1
    }
    */
    @SerializedName("how2go")
    private ArrayList<ToGo> how2Go;
    @SerializedName("processInfo")
    private String processInfo;

    public ArrayList<ToGo> getHow2Go() {
        return how2Go;
    }
    public String getProcessInfo() {
        return processInfo;
    }

    @Override
    public String toString() {
        return "how2goDetail{"             +   '\''          +
                super.toString()           +  '\''           +
                "how2go"                   + how2Go          +  '\''  +
                "processInfo"              + processInfo     +  '\''  +
                '}';
    }

}
