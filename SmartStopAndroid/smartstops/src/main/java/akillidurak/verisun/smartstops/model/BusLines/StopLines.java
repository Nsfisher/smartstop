package akillidurak.verisun.smartstops.model.BusLines;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 17/02/17.
 */

public class StopLines {

    /****
     * {
     "lineId": 125789,
     "lineCode": "439",
     "name": "PURSAKLAR-AYYILDIZ-ULUS-SIHHİYE-BAKANLIKLAR"
     }
     */

    @SerializedName("lineId")
    private int lineId;

    @SerializedName("lineCode")
    private String lineCode;

    @SerializedName("name")
    private String name;

    public int getLineId() {
        return lineId;
    }

    public String getLineCode() {
        return lineCode;
    }

    public String getName() {
        return name;
    }
}
