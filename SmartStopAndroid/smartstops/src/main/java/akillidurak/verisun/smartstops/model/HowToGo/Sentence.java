package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 6/5/17.
 */

public class Sentence {
    /***
     * {
     "processInfo":"1",
     "data":[]
     }
     */




    @SerializedName("processInfo")
    private String processInfo;

    @SerializedName("data")
    private ArrayList<SentecesData> data;

    public ArrayList<SentecesData> getData() {
        return data;
    }

    public String getProcessInfo() {
        return processInfo;
    }
}
