package akillidurak.verisun.smartstops.model.GetBusline;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 14/02/17.
 */

public class GetBuslineInfo {

    /***
     * {
     "busLineInfo":{},
     "processInfo":1
     }
     */

    @SerializedName("busLineInfo")
    private BusLineInfo buslineInfo;

    @SerializedName("processInfo")
    private int processInfo;

    public BusLineInfo getBuslineInfo() {
        return buslineInfo;
    }

    public int getProcessInfo() {
        return processInfo;
    }
}
