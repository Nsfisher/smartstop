package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 */
public class Stats  {
    @SerializedName("points")
    private Double points;

    @SerializedName("duration")
    private int duration;

    @SerializedName("hops")
    private int hops;

    public Double getPoints() {
        return points;
    }

    public void setPoints(Double points) {
        this.points = points;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getHops() {
        return hops;
    }

    public void setHops(int hops) {
        this.hops = hops;
    }

    @Override
    public String toString() {
        return "Stats{"                       +  '\''               +
                super.toString()              +  '\''               +
                "points"                      + points               +  '\''  +
                "duration"                    + duration             +  '\''  +
                "hops"                        + hops                 +  '\''  +
                '}';
    }



}
