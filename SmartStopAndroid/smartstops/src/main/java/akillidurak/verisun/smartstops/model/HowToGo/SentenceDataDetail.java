package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 6/15/17.
 */

public class SentenceDataDetail {

    @SerializedName("sentence")
    private String sentence;

    @SerializedName("path")
    private Path path;

    public Path getPath() {
        return path;
    }

    public String getSentence() {
        return sentence;
    }
}
