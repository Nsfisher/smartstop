package akillidurak.verisun.smartstops.model.GetBusline;





import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;

/**
 * Created by nsfisher on 14/02/17.
 */

public class LiveBuses {
    /****
     * "liveBuses":[
     {
     "nextStopId":"52246",
     "nextStopName":"YUNUS EMRE SİTESİ",
     "previousStopId":"52250",
     "previousStopName":"İNCİRLİ LİSESİ",
     "timeToNearestStop":10,
     "plate":"06 EA 665",
     "speed":39,
     "location":{
     "latitude":39.980698,
     "longitude":32.848301
     }
     }
     ]
     */
    @SerializedName("nextStopId")
    private String nextStopId;

    @SerializedName("nextStopName")
    private String nextStopName;

    @SerializedName("previousStopId")
    private String previousStopId;

    @SerializedName("previousStopName")
    private String previousStopName;

    @SerializedName("timeToNearestStop")
    private int timeToNearestStop;

    @SerializedName("plate")
    private String plate;

    @SerializedName("speed")
    private int speed;

    @SerializedName("location")
    private Locations location;


    public int getSpeed() {
        return speed;
    }

    public int getTimeToNearestStop() {
        return timeToNearestStop;
    }

    public Locations getLocation() {
        return location;
    }

    public String getNextStopId() {
        return nextStopId;
    }

    public String getNextStopName() {
        return nextStopName;
    }

    public String getPlate() {
        return plate;
    }

    public String getPreviousStopId() {
        return previousStopId;
    }

    public String getPreviousStopName() {
        return previousStopName;
    }

}
