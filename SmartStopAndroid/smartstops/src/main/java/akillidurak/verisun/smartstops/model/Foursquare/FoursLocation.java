package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursLocation {
    /**
     "location":{
        "address":"119 Sullivan St",
                "crossStreet":"btwn Prince & Spring St",
                "lat":40.726161490315945,
                "lng":-74.0026326791738,
                "distance":2920,
                "postalCode":"10012",
                "cc":"US",
                "city":"New York",
                "state":"NY",
                "country":"ABD",
                "formattedAddress":[
        "119 Sullivan St (btwn Prince & Spring St)",
                "New York, NY 10012",
                "ABD"
        ]
            },
     **/
    /*
    @SerializedName("address")
    private String address;

    @SerializedName("crossStreet")
    private String crossStreet ;

    @SerializedName("postalCode")
    private String postalCode;

    @SerializedName("cc")
    private  String cc;
    */
    @SerializedName("lat")
    private Double latitude;

    @SerializedName("lng")
    private Double longitude;

    @SerializedName("distance")
    private int distance;



    @SerializedName("city")
    private String city;

    @SerializedName("state")
    private String state;

    @SerializedName("country")
    private String country;

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public int getDistance() {
        return distance;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getState() {
        return state;
    }


    public String toString() {
        return "FoursLocation{"             +                  + '\''   +
                super.toString()            +                  + '\''   +
                "lat"                       +     latitude     +  '\''  +
                "lng"                       +     longitude    +  '\''  +
                "distance"                  +     distance     +  '\''  +
                "city"                      +     city         +  '\''  +
                "state"                     +     state        +  '\''  +
                "country"                   +     country      +  '\''  +
                '}';
    }


}
