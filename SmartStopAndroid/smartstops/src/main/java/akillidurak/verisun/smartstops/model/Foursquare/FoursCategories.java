package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursCategories  {
    /***
    "categories":[
    {
        "id":"4bf58dd8d48988d1d2941735",
            "name":"Suşi Restoranı",
            "pluralName":"Suşi Restoranları",
            "shortName":"Suşi",
            "icon":{
        "prefix":"https://ss3.4sqi.net/img/categories_v2/food/sushi_",
                "suffix":".png"
    },
        "primary":true
    }
    ],

     **/
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("pluralName")
    private String pluralName;

    @SerializedName("shortName")
    private String shortName;


}
