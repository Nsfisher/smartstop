package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursVenuesArray  {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;
    /*
    @SerializedName("contact")
    private FoursContact contact;
     */
    @SerializedName("location")
    private FoursLocation foursLocation;
    /*
    @SerializedName("categories")
    private ArrayList<FoursCategories> categories;

    @SerializedName("url")
    private String url;

    get
     */

    public FoursLocation getFoursLocation() {
        return foursLocation;
    }

    public String toString() {
        return "FoursVenuesArray{"                              +  '\''  +
                super.toString()           +                    +  '\''  +
                "name"                     +   name             +  '\''  +
                "location"                 +   foursLocation    +  '\''  +
                '}';
    }

}
