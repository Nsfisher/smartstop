package akillidurak.verisun.smartstops.model.BusStopLocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 */


/**
{
        "processInfo":1,
        "busStops":[
        {
        "code":"402221",
        "county":"Kadıköy",
        "location":{
        "latitude":40.998787,
        "longitude":29.056355
        },
        "name":"UZUNÇAYIR METROBÜS",
        "type":"MODERN",
        "id":"70588",
        "distanceToPoint":43

        },
        {
        "code":"402221",
        "county":"Kadıköy",
        "location":{
        "latitude":40.998787,
        "longitude":29.056355
        },
        "name":"UZUNÇAYIR E-5",
        "type":"MODERN",
        "id":"70588",
        "distanceToPoint":234
        }
        ]
        }

 */
public class BusStopLocationInfo  {

    @SerializedName("processInfo")
    private String processInfo;

    @SerializedName("busStops")
    private ArrayList<BusStops> busStopses;

    public String getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(String processInfo) {
        this.processInfo = processInfo;
    }

    public ArrayList<BusStops> getBusStopses() {
        return busStopses;
    }

    public void setBusStopses(ArrayList<BusStops> busStopses) {
        this.busStopses = busStopses;
    }




    @Override
    public String toString() {
        return "BusStopLocationInfo{"            +  '\''  +
                super.toString()        +  '\''  +
                "processInfo"            + processInfo     +  '\''  +
                "busStops"               + busStopses      +  '\''  +
                '}';
    }
}
