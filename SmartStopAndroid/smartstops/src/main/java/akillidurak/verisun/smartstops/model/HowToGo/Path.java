package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 */
public class Path {


    @SerializedName("lineId")
    private String lineID;

    @SerializedName("line")
    private String line;

    @SerializedName("name")
    private String name;

    @SerializedName("time")
    private String time;

    @SerializedName("duration")
    private int duration;

    @SerializedName("code")
    private String code;

    @SerializedName("type")
    private String type;

    @SerializedName("action")
    private String action;

    @SerializedName("lat")
    private Double latitude;

    @SerializedName("lng")
    private Double longitude;

    @SerializedName("nodes")
    private ArrayList<Nodes> nodes;

    public String getLineID() {
        return lineID;
    }

    public String getLine() {
        return line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Nodes> getNodes() {
        return nodes;
    }
    public void setNodes(ArrayList<Nodes> nodes) {
        this.nodes = nodes;
    }

    @Override
    public String toString() {
        return "Search{"                   +   '\''                +
                super.toString()           +  '\''                 +
                "name"                      + name           +  '\''  +
                "time"                    + time               +  '\''  +
                "duration"                 + duration        +  '\''  +
                "code"                   + code                +  '\''  +
                "type"                   + type                +  '\''  +
                "action"                   + action                +  '\''  +
                "lat"                   + latitude                +  '\''  +
                "lng"                   + longitude                +  '\''  +

                '}';
    }
}
