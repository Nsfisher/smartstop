package akillidurak.verisun.smartstops.model.VehicleLocation;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 25/05/16.
 */
public class VehicleInfo  {

   /**
    "vehicleInfo":[
    {
    "speed":32,
    "plaka":"06 EA 666",
    "name":"KIZILAY - OTOGAR",
    "location":{
    "latitude":"39.921104,",
    "longitude":"32.852666"
    },
    "lineInfo":{
    "lineCode":"E-11",
    "lineId":34765,
    "nearestStop":{
    "code":"402031",
    "name":"KADIKÖY",
    "directionDescription":"RIHTIM1 PERON",
    "timeToStop":5,

    "id":"70572"
    }
    }
    }
     **/




    @SerializedName("speed")
    private String speed;

    @SerializedName("plaka")
    private String plaka;

    @SerializedName("location")
    private Locations locations;

    @SerializedName("name")
    private String name;



    @SerializedName("lineInfo")
    private  LineInfo lineInfo;

 public String getSpeed() {
  return speed;
 }

 public void setSpeed(String speed) {
  this.speed = speed;
 }

 public Locations getLocations() {
  return locations;
 }

 public void setLocations(Locations locations) {
  this.locations = locations;
 }
    public String getName() {
        return name;
    }

    public LineInfo getLineInfo() {
  return lineInfo;
 }

 public void setLineInfo(LineInfo lineInfo) {
  this.lineInfo = lineInfo;
 }

 public String getPlaka() {
  return plaka;
 }



 //json response
    @Override
    public String toString() {
        return "VehicleInfo{"         +  '\''  +
                super.toString()      +  '\''  +

                "speed"               + speed           +  '\''  +
                "location"            + locations       +
               '}';
    }



}
