package akillidurak.verisun.smartstops.util;

import com.google.gson.Gson;

import akillidurak.verisun.smartstops.model.SearchMap.Search;

/**
 * Created by nsfisher on 27/05/16.
 */
public class SetSearch  {

    private Search search;
    private Gson  gson;
    private String strJson;

    public Search init()    {

        search = new Search();
        gson = new Gson();

        strJson =" { \"processInfo\" :1, \"busLines\" :[ { \"code\" :\"B1\" , \"journeyDuration\" :105, \"name\" :\"BURÇ KAVŞAĞI-IBRAHIMLI-GAZIKENT\" , " +
                "\"directions\" :[ \"DEPARTURE\" ], \"id\" :\"231\" }, { \"code\" :\"B10\" , \"journeyDuration\" :35, \"name\" :\"BALIKLI-H.BAHÇESI\" , " +
                "\"directions\" :[ \"DEPARTURE\" ], \"id\" :\"27\" } ], \"busStops\" :[ { \"code\" :\"1268\" , \"latitude\" :37.0476, \"longitude\" :37.36861, " +
                "\"name\" :\"KURBANBABA GENÇLIK MERKEZI\" , \"id\" :\"295\" }, { \"code\" :\"16\" , \"latitude\" :37.10642, \"longitude\" :37.4034, " +
                "\"name\" :\"M1. MERKEZ AVM.\" , \"id\" :\"651\" } ], \"adress\" :[]}";

        search=gson.fromJson(strJson,Search.class);

      return search;
    }
}
