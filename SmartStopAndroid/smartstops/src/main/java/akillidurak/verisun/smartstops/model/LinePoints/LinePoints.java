package akillidurak.verisun.smartstops.model.LinePoints;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;

/**
 * Created by nsfisher on 7/20/17.
 */

public class LinePoints {
    /***
     * {
     "processInfo":"1",
     "lineCoordinates":[]
     }
     */


    @SerializedName("processInfo")
    private String processInfo;
    @SerializedName("lineCoordinates")
    private ArrayList<Locations> lineCoordinates;

    public ArrayList<Locations> getLineCoordinates() {
        return lineCoordinates;
    }

    public String getProcessInfo() {
        return processInfo;
    }
}
