package akillidurak.verisun.smartstops.model;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 25/05/16.
 * Json Map haritasini Loglamak icin kullanılır. id-->Busline description-->buslineId
 */
public  abstract  class BaseModel {

    @SerializedName("ids")
    String ids;

    @SerializedName("description")
    String  description;

    public String getId() {
        return ids;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ids='" + ids + '\'' +
                ", description='" + description + '\'';
    }

}
