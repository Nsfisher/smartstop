package akillidurak.verisun.smartstops.util;

import com.google.gson.Gson;

import akillidurak.verisun.smartstops.model.VehicleLocation.LineInfo;
import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;
import akillidurak.verisun.smartstops.model.VehicleLocation.NearestStop;
import akillidurak.verisun.smartstops.model.VehicleLocation.VehicleInfo;
import akillidurak.verisun.smartstops.model.VehicleLocation.VehicleLocationInfo;

/**
 * Created by nsfisher on 26/05/16.
 */
public class SetVehicleLocation {

    private VehicleLocationInfo vehicleLocationInfo;
    private VehicleInfo vehicleInfo;
    private Locations locations;
    private LineInfo lineInfo;
    private NearestStop nearestStop;
    private Gson gson;
    private String strJson;
    public  VehicleLocationInfo  init() {
      strJson="{\n" +
              "   \"processInfo\":1,\n" +
              "   \"vehicleInfo\":[\n" +
              "      {\n" +
              "         \"vehicleType\":\"bus\",\n" +
              "         \"speed\":32,\n" +
              "         \"location\":{\n" +
              "            \"latitude\":\"40.996971\",\n" +
              "            \"longitude\":\"29.026369\"\n" +
              "         },\n" +
              "         \"distanceToPoint\":\"230\",\n" +
              "         \"lineInfo\":{\n" +
              "            \"lineCode\":\"E-11\",\n" +
              "            \"lineId\":34765,\n" +
              "            \"nearestStop\":[\n" +
              "               {\n" +
              "                  \"code\":\"402031\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.99295,\n" +
              "                  \"longitude\":29.024071,\n" +
              "                  \"name\":\"KADIKÖY\",\n" +
              "                  \"directionDescription\":\"RIHTIM1 PERON\",\n" +
              "                  \"id\":\"70572\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225541\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.996971,\n" +
              "                  \"longitude\":29.026369,\n" +
              "                  \"name\":\"HAMZA YERLİKAYA\",\n" +
              "                  \"directionDescription\":\"E-5\",\n" +
              "                  \"id\":\"69391\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"207071\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.999371,\n" +
              "                  \"longitude\":29.03265,\n" +
              "                  \"name\":\"AYRILIKÇEŞME\",\n" +
              "                  \"directionDescription\":\"E-5\",\n" +
              "                  \"id\":\"65896\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225563\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":41.003132,\n" +
              "                  \"longitude\":29.0348,\n" +
              "                  \"name\":\"KOŞUYOLU KÖPRÜSÜ\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69397\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225581\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":41.003029,\n" +
              "                  \"longitude\":29.03878,\n" +
              "                  \"name\":\"ACIBADEM LİSESİ\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69401\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225591\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":41.002331,\n" +
              "                  \"longitude\":29.042509,\n" +
              "                  \"name\":\"ACIBADEM KÖPRÜSÜ\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69402\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225601\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":41.00172,\n" +
              "                  \"longitude\":29.045679,\n" +
              "                  \"name\":\"ACIBADEM METRO İST.\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69403\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"402221\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.998787,\n" +
              "                  \"longitude\":29.056355,\n" +
              "                  \"name\":\"UZUNÇAYIR METROBÜS\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"70588\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"229682\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.99555,\n" +
              "                  \"longitude\":29.06473,\n" +
              "                  \"name\":\"İST.MEDENİYET ÜNV.\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"70213\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225622\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.99284,\n" +
              "                  \"longitude\":29.071711,\n" +
              "                  \"name\":\"GÖZTEPE KÖPRÜSÜ\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69406\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"206492\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.990349,\n" +
              "                  \"longitude\":29.078569,\n" +
              "                  \"name\":\"BAHÇELER\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"65789\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"225632\",\n" +
              "                  \"county\":\"Kadıköy\",\n" +
              "                  \"latitude\":40.986179,\n" +
              "                  \"longitude\":29.088261,\n" +
              "                  \"name\":\"YENİSAHRA\",\n" +
              "                  \"directionDescription\":\"TUZLA\",\n" +
              "                  \"id\":\"69408\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"210331\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.92981,\n" +
              "                  \"longitude\":29.32596,\n" +
              "                  \"name\":\"OTOYOL KAVŞAĞI\",\n" +
              "                  \"directionDescription\":\"PENDİK\",\n" +
              "                  \"id\":\"66515\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"206652\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.929081,\n" +
              "                  \"longitude\":29.32336,\n" +
              "                  \"name\":\"HARMANDERE\",\n" +
              "                  \"directionDescription\":\"PENDİK\",\n" +
              "                  \"id\":\"65821\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"212542\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.927528,\n" +
              "                  \"longitude\":29.319019,\n" +
              "                  \"name\":\"H.DERE MESLEK LİSESİ\",\n" +
              "                  \"directionDescription\":\"PENDİK\",\n" +
              "                  \"id\":\"66932\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"211812\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.926361,\n" +
              "                  \"longitude\":29.315821,\n" +
              "                  \"name\":\"BENZİNCİ\",\n" +
              "                  \"directionDescription\":\"PENDİK\",\n" +
              "                  \"id\":\"66810\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"207543\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.923939,\n" +
              "                  \"longitude\":29.312771,\n" +
              "                  \"name\":\"HAVAALANI KAVŞAĞI\",\n" +
              "                  \"directionDescription\":\"HAVALİMANI\",\n" +
              "                  \"id\":\"65980\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"210082\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.91217,\n" +
              "                  \"longitude\":29.31324,\n" +
              "                  \"name\":\"S.GÖKÇEN NİZAMİYE\",\n" +
              "                  \"directionDescription\":\"HAVALİMANI\",\n" +
              "                  \"id\":\"66467\"\n" +
              "               },\n" +
              "               {\n" +
              "                  \"code\":\"210091\",\n" +
              "                  \"county\":\"Pendik\",\n" +
              "                  \"latitude\":40.90863,\n" +
              "                  \"longitude\":29.315029,\n" +
              "                  \"name\":\"SABİHA GÖKÇEN H.L.\",\n" +
              "                  \"directionDescription\":\"(PERON)\",\n" +
              "                  \"id\":\"66468\"\n" +
              "               }\n" +
              "            ]\n" +
              "         }\n" +
              "      }\n" +
              "   ]\n" +
              "}";
        vehicleLocationInfo = new VehicleLocationInfo();
        vehicleInfo = new VehicleInfo();
        locations = new Locations();
        nearestStop = new NearestStop();
        lineInfo = new LineInfo();
        gson = new Gson();
        vehicleLocationInfo = gson.fromJson(strJson, VehicleLocationInfo.class);
        //setVehicleLocationInfo();
        //setVehicleInfo();
        //setLineInfo();
         return vehicleLocationInfo;

    }




    public void setVehicleLocationInfo() {

        vehicleLocationInfo.setVehicleInfos(vehicleLocationInfo.getVehicleInfos());
        vehicleLocationInfo.setProcessInfo(vehicleLocationInfo.getProcessInfo());
    }

 /*   public void setVehicleInfo(){

        vehicleInfo.setDistanceToPoint(vehicleLocationInfo.getVehicleInfos().get(0).getDistanceToPoint());
        vehicleInfo.setVehicleType(vehicleLocationInfo.getVehicleInfos().get(0).getVehicleType());
        vehicleInfo.setLineInfo(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo());
        vehicleInfo.setSpeed(vehicleLocationInfo.getVehicleInfos().get(0).getSpeed());
        vehicleInfo.setLocations(vehicleLocationInfo.getVehicleInfos().get(0).getLocations());
    }*/
    public void setLineInfo() {
        lineInfo.setStopses(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo().getStopses());
        lineInfo.setLineCode(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo().getLineCode());
        lineInfo.setLineId(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo().getLineId());
    }

    /*
    public void setLocation()
    {

        locations.setLongitude(vehicleLocationInfo.getVehicleInfos().get(0).getLocations().getLongitude());
        locations.setLatitude(vehicleLocationInfo.getVehicleInfos().get(0).getLocations().getLatitude());
    }

    public void setStops(){
        for (int i = 0; i < 5; i++) {
            nearestStop.setDirectionDescription(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo().getStopses().get(i).getDirectionDescription());
        }
        Log.e("asd","asd") ;

        nearestStop.setDirectionDescription(vehicleLocationInfo.getVehicleInfos().get(0).getLineInfo().getStopses().get());
        nearestStop.setLatitude(resStops.getLatitude());
        nearestStop.setLongitude(resStops.getLongitude());
        nearestStop.setCode(resStops.getCode());
        nearestStop.setCounty(resStops.getCounty());
        nearestStop.setName(resStops.getName());

    }*/

}

