package akillidurak.verisun.smartstops.model.LineStops;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;

/**
 * Created by nsfisher on 17/02/17.
 */

public class LineStops {

    /****
     * {
     "code":"12573",
     "name":"GÖLBAŞI HAREKET NOKTASI",
     "directionDescription":"",
     "id":51284,
     "sequenceNo":0,
     "location":{
     "latitude":39.783035,
     "longitude":32.811321
     }
     }
     */

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("directionDescription")
    private String directionDescription;

    @SerializedName("id")
    private int id;

    @SerializedName("sequenceNo")
    private String sequenceNo;

    @SerializedName("location")
    private Locations locations;

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public Locations getLocations() {
        return locations;
    }

    public String getCode() {
        return code;
    }

    public String getDirectionDescription() {
        return directionDescription;
    }

    public String getSequenceNo() {
        return sequenceNo;
    }

}
