package akillidurak.verisun.smartstops.core;

import android.content.Context;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.orhanobut.wasp.Wasp;
import com.orhanobut.wasp.parsers.GsonParser;
import com.orhanobut.wasp.utils.AuthToken;
import com.orhanobut.wasp.utils.LogLevel;
import com.orhanobut.wasp.utils.RequestInterceptor;
import com.orhanobut.wasp.utils.WaspRetryPolicy;
import com.squareup.okhttp.OkHttpClient;

import java.util.Map;

import akillidurak.verisun.smartstops.BuildConfig;


/**
 * Created by nsfisher on 24/06/16.
 */
public class App extends BaseApp {

    public static Context context;
    private OkHttpClient okHttpClient;
    private static  Gson gson;
    public static App instance;
    @Override
    public void onCreate() {
        super.onCreate();
      //  Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
       instance = this;
        //setInstance(instance);
        context = this;
        this.okHttpClient = new OkHttpClient();
        this.gson = new GsonBuilder()
                .setDateFormat("dd.MM.yy HH:mm:ss")
                //.registerTypeAdapter(Searchable.class, new SearchItemDeserializer())
                .create();

    }
    public static Gson getGson() {
        return gson;
    }

    public OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }

    public App() {

    }
    public App(Context context) {
        attachBaseContext(context);
    }
}

