package akillidurak.verisun.smartstops.util;

import com.google.gson.Gson;

import akillidurak.verisun.smartstops.model.BusStopMapping.Bus;
import akillidurak.verisun.smartstops.model.BusStopMapping.BusStopInfo;

/**
 * Created by nsfisher on 27/05/16.
 */
public class SetBusStopMapping {

    private BusStopInfo busStopInfo;
    private Gson gson;
    private String strJson;
    public BusStopInfo init(){
        busStopInfo = new BusStopInfo();
        gson = new Gson();

        strJson ="{ \"processInfo\" :1, \"stopType\" :\"BUS\" , \"code\" :\"402221\" , \"county\" :\"Kadıköy\" , \"location\" :{ \"latitude\" :40.998787, \"longitude\" :29.056355 }, \"name\" :\"UZUNÇAYIR METROBÜS\" , \"buses\" :[ { \"code\" :\"B43\" , \"journeyDuration\" :100, \"name\" :\"IPEKEVLER-SEFAŞEHIR\" , \"type\" :\"1\" , \"directions\" :[ \"DEPARTURE\" ], \"id\" :\"112\" , \"timeToStop\" :15, \"stopCount\" :4, \"distanceToStop\" :3.3963895870775325 }, { \"code\" :\"B43\" , \"journeyDuration\" :100, \"name\" :\"SEFAŞEHIR-IPEKEVLER\" , \"type\" :\"1\" , \"directions\" :[ \"DEPARTURE\" ], \"id\" :\"273\" , \"timeToStop\" :19, \"stopCount\" :1, \"distanceToStop\" :0 } ]}";
        busStopInfo=gson.fromJson(strJson,BusStopInfo.class);
        return  busStopInfo;
    }
}
