package akillidurak.verisun.smartstops.model.TimeTable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.SearchMap.BusLine;

/**
 * Created by nsfisher on 20/12/16.
 */

public class TimeTable {
    /***
     * "timeTable":{
     *  "busLine":{},
     *  "lineSchedules":[],
     *  "descriptions":[
     *  ]
     *  }
     */

   // private BusLine busLine;
    @SerializedName("lineSchedules")
    private LineSchecule lineSchedules;

    public LineSchecule getLineSchedules() {
        return lineSchedules;
    }
}
