package akillidurak.verisun.smartstops.model.TimeTable;

import com.google.gson.annotations.SerializedName;



/**
 * Created by nsfisher on 06/02/17.
 */

public class Adress  {
    /***
     * "adress":[
     * {
     * "latitude":37.10642,
     * "longitude":37.4034,
     * "name":"M1. MERKEZ AVM."
     * "id": "fe907038d84fb31e400cc2d09b812cbf8245cd5e"
     * }
     * ],
     */

    @SerializedName("latitude")
    private Double latitude;

    @SerializedName("longitude")
    private Double longitude;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
