package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 6/5/17.
 */

public class SentecesData {


    /***
     *
     * {
     "sentence":"ÇİFTLİK KAVŞAĞI Durağına yürüyün."
     }
     */


    @SerializedName("sentenceArray")
    private ArrayList<SentenceDataDetail> dataDetails;



    public ArrayList<SentenceDataDetail> getDataDetails() {
        return dataDetails;
    }

    @SerializedName("stats")
    private Stats stats;

    @SerializedName("vehicles")
    private Vehicles vehicles;

    public Vehicles getVehicles() {
        return vehicles;
    }

    public Stats getStats() {
        return stats;
    }


}
