package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursData  {
    @SerializedName("response")
    private  FoursVenues response;

    public FoursVenues getResponse() {
        return response;
    }
    @Override
    public String toString() {
        return "FoursData{"                   +   '\''                +
                super.toString()           +  '\''                 +
                "response"                      + response           +  '\''  +
                '}';
    }
}
