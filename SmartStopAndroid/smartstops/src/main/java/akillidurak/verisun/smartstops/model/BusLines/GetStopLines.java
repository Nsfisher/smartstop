package akillidurak.verisun.smartstops.model.BusLines;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 17/02/17.
 */

public class GetStopLines {

    @SerializedName("stopLines")
    private ArrayList<StopLines> busLines;

    public ArrayList<StopLines> getBusLines() {
        return busLines;
    }

}
