package akillidurak.verisun.smartstops.core;

import android.util.Base64;
import android.util.Log;

import com.google.gson.JsonObject;
import com.orhanobut.wasp.Callback;
import com.orhanobut.wasp.Response;
import com.orhanobut.wasp.Wasp;
import com.orhanobut.wasp.WaspError;
import com.orhanobut.wasp.WaspRequest;
import com.orhanobut.wasp.http.Body;
import com.orhanobut.wasp.http.GET;
import com.orhanobut.wasp.http.POST;
import com.orhanobut.wasp.http.Path;
import com.orhanobut.wasp.http.Query;
import com.orhanobut.wasp.parsers.GsonParser;
import com.orhanobut.wasp.utils.AuthToken;
import com.orhanobut.wasp.utils.LogLevel;
import com.orhanobut.wasp.utils.RequestInterceptor;
import com.orhanobut.wasp.utils.WaspRetryPolicy;


import org.json.JSONObject;

import java.util.Map;

import akillidurak.verisun.smartstops.BuildConfig;
import akillidurak.verisun.smartstops.R;
import akillidurak.verisun.smartstops.model.BusLines.GetStopLines;
import akillidurak.verisun.smartstops.model.BusStopLocation.BusStopLocationInfo;
import akillidurak.verisun.smartstops.model.BusStopMapping.Bus;
import akillidurak.verisun.smartstops.model.BusStopMapping.BusStopInfo;


import akillidurak.verisun.smartstops.model.Foursquare.FoursData;
import akillidurak.verisun.smartstops.model.GetBusline.GetBuslineInfo;
import akillidurak.verisun.smartstops.model.HowToGo.How2Go;
import akillidurak.verisun.smartstops.model.HowToGo.Sentence;
import akillidurak.verisun.smartstops.model.LinePoints.LinePoints;
import akillidurak.verisun.smartstops.model.LineStops.GetLineStops;
import akillidurak.verisun.smartstops.model.SearchMap.Search;
import akillidurak.verisun.smartstops.model.TimeTable.TimeTable;
import akillidurak.verisun.smartstops.model.TimeTable.TimeTableModel;
import akillidurak.verisun.smartstops.model.VehicleLocation.VehicleLocationInfo;

/**
 * Created by nsfisher on 24/05/16.
 */
public interface Service {


    /***
     * BaseURL  = "http://213.14.60.254:8443/BoluServices/HatAracServiceJson.svc "
     * username = "bolumobilapp "
     * password = "bolumobilapp"
     */

   // String BaseURL = "http://verisun.com:8543/ekentankara/rest";
    String BaseURL = "https://verisun.com:8543/ekentankara/rest";
    String username = "android";
    String password = "EQcgGLecuVMuiV0B46gO";

    String FoursquareURL="https://api.foursquare.com/v2/venues/search?client_id=3BYJLICNW5OK0NZSHXZMU2KER1WVX13FRJLBKJSNZ30SSUX2&client_secret=KEOZRAK2QNGOARFOZNBS0OMSD4Q5KW5GAWAQFZVMVI41TNRY&v=20130815";
    Service service = new Wasp.Builder(App.context)
            .setEndpoint(Service.BaseURL)
            .setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void onHeadersAdded(Map<String, String> stringStringMap) {
                }

                @Override
                public void onQueryParamsAdded(Map<String, Object> stringObjectMap) {

                }

                @Override
                public WaspRetryPolicy getRetryPolicy() {
                    return null;
                }

                @Override
                public AuthToken getAuthToken() {
                    return new AuthToken("Basic " + Base64.encodeToString((Service.username + ":" + Service.password).getBytes(), Base64.NO_WRAP));
                }
            })
            //.trustCertificates()
            .setWaspHttpStack(new SmartStopsOkHttpStack(App.instance,R.raw.smart_stop_cert,"d68G0911KAT39Nf3l6g304ds585q765T"))
            .setParser(new GsonParser(App.getGson()))
            .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
            .build()
            .create(Service.class);



        Service foursquareService = new Wasp.Builder(App.context)
            .setEndpoint(Service.FoursquareURL)
            .setRequestInterceptor(new RequestInterceptor() {
                @Override
                public void onHeadersAdded(Map<String, String> stringStringMap) {
                }

                @Override
                public void onQueryParamsAdded(Map<String, Object> stringObjectMap) {

                }

                @Override
                public WaspRetryPolicy getRetryPolicy() {
                    return null;
                }

                @Override
                public AuthToken getAuthToken() {
                    return null;
                }
            })
            .trustCertificates()
            .setParser(new GsonParser(App.getGson()))
            .setLogLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE)
            .build()
            .create(Service.class);

    //FOR HOW2GO
    //http://verisun.com:8181/ekentankara/rest/how2go/getData?startLatitude=10.1&startLongitude=10.1&destinationLatitude=123&destinationLongitude=123&vehicleTypes=123&time=123&cityId=1
    @GET("/how2go/getData?startLatitude={startLatitude}&startLongitude={startLongitude}&destinationLatitude={destinationLatitude}&destinationLongitude={destinationLongitude}&vehicleTypes={vehicleTypes}&time={time}&cityId={cityId}")
    WaspRequest getHowtoGoDetail(@Path("startLatitude") String startLatitude, @Path("startLongitude") String startLongitude, @Path("destinationLatitude") String destinationLatitude,
                          @Path("destinationLongitude") String destinationLongitude, @Path("vehicleTypes") String vehicleTypes, @Path("time") String time,@Path("cityId") String cityId,
                          BaseCallBack<How2Go> callback);

    //FOR   getVehicleLocationInfo
  //  http://verisun.com:8181/ekentankara/rest/buslines/getVehicleLocationInfo?topLeftlatitude=1&topLeftlongitude=1&topRightLatiude=1&topRightLongitude=1&bottomLeftLatitude=1&bottomLeftLongitude=1&bottomRightLatitude=1&bottomRightLongitude=1&type=1&cityId=1
    @GET("/buslines/getVehicleLocationInfo?topLeftLatitude={topLeftLatitude}&topLeftLongitude={topLeftLongitude}&topRightLatitude={topRightLatitude}&topRightLongitude={topRightLongitude}" +
            "&bottomLeftLatitude={bottomLeftLatitude}&bottomLeftLongitude={bottomLeftLongitude}&bottomRightLatitude={bottomRightLatitude}&bottomRightLongitude={bottomRightLongitude}" +
            "&cityId={cityId}")
    WaspRequest getVehicleLocationInfo(@Path("topLeftLatitude") String topLeftlatitude, @Path("topLeftLongitude")String topLeftlongitude, @Path("topRightLatitude") String topRightLatitude
                                       , @Path("topRightLongitude") String topRightLongitude, @Path("bottomLeftLatitude") String bottomLeftLatitude, @Path("bottomLeftLongitude") String bottomLeftLongitude
                                       , @Path("bottomRightLatitude") String bottomRightLatitude, @Path("bottomRightLongitude") String bottomRightLongitude
                                       , @Path("cityId") String cityId, BaseCallBack<VehicleLocationInfo> callBack) ;

    //https://verisun.com:8543/ekentankara/rest/how2go/getHowToGoSentence?startLatitude=39.949&startLongitude=32.788&destinationLatitude=40.124473&destinationLongitude=32.991726&lang=tr
    //https://verisun.com:8543/ekentankara/rest/how2go/getHowToGoSentence?startLatitude=39.949&startLongitude=32.788&destinationLatitude=40.124473&destinationLongitude=32.991726&lang=tr
    //https://verisun.com:8543/ekentankara/rest/how2go/getHowToGoSentence?startLatitude=39.949&startLongitude={startLongitude}&destinationLatitude=40.124473&destinationLongitude=32.991726&lang=tr
    @GET("/how2go/getHowToGoSentence?startLatitude={startLatitude}&startLongitude={startLongitude}&destinationLatitude={destinationLatitude}&destinationLongitude={destinationLongitude}&lang={tr}")
    WaspRequest  getHowToGoSentence(@Path("startLatitude") double startLatitude, @Path("startLongitude") double startLongitude, @Path("destinationLatitude") double destinationLatitude, @Path("destinationLongitude")double destinationLongitude, @Path("tr") String tr, BaseCallBack<Sentence> callBack);


    //http://verisun.com:8181/ekentankara/rest/search/keywords?searchString=1&cityId=1
    @GET("/search/keywords?searchString={searchString}&cityId={cityId}&busStops={busStops}&adress={adress}&busLines={busLines}")
    WaspRequest getSearch(@Path("searchString") String searchString, @Path("cityId") String cityId, @Path("busStops") int busStops, @Path("adress") int adress, @Path("busLines") int buslines, BaseCallBack<Search> callBack);

    //http://verisun.com:8181/ekentankara/rest/busstop/getBusStop?stopId=1&cityId=1

    @GET("/busstop/getBusStop?stopId={stopId}&cityId={cityId}")
    WaspRequest getBusStop(@Path("stopId")int stopId,@Path("cityId") String cityId, BaseCallBack<BusStopInfo> callback);

    @GET("/busstop/getBusStopLocationInfo?topLeftLatitude={topLeftLatitude}&topLeftLongitude={topLeftLongitude}&topRightLatitude={topRightLatitude}&topRightLongitude={topRightLongitude}&" +
            "bottomLeftLatitude={bottomLeftLatitude}&" + "bottomLeftLongitude={bottomLeftLongitude}&bottomRightLatitude={bottomRightLatitude}&bottomRightLongitude={bottomRightLongitude}&" +
            "cityId={cityId}")
     WaspRequest getBusStopLocationInfo(@Path("topLeftLatitude") String topLeftLatitude,@Path("topLeftLongitude")String topLeftLongitude,@Path("topRightLatitude") String topRightLatitude
                                        ,@Path("topRightLongitude") String topRightLongitude,@Path("bottomLeftLatitude") String bottomLeftLatitude,@Path("bottomLeftLongitude") String bottomLeftLongitude
                                        ,@Path("bottomRightLatitude") String bottomRightLatitude,@Path("bottomRightLongitude") String bottomRightLongitude
                                        ,@Path("cityId") String cityId,BaseCallBack<BusStopLocationInfo> callBack);

    //https://api.foursquare.com/v2/venues/search?client_id=3BYJLICNW5OK0NZSHXZMU2KER1WVX13FRJLBKJSNZ30SSUX2&client_secret=KEOZRAK2QNGOARFOZNBS0OMSD4Q5KW5GAWAQFZVMVI41TNRY&v=20130815&ll=40.7,-74&query=sushi

   @GET("&ll={latitude},{longitude}&query={keyword}")
   WaspRequest getFoursquare(@Path("latitude") Double latitude, @Path("longitude")Double longitude, @Path("keyword") String keyword, BaseCallBack<FoursData> callBack) ;


    @GET("/timetable/getTimeTableNew")
    WaspRequest getTimeTable(@Query("lineId") String code, @Query("cityId") String cityId, BaseCallBack<TimeTableModel> callBack);


    @GET("/buslines/getBusLine")
    WaspRequest getBusLine(@Query("lineId") int lineId, BaseCallBack<GetBuslineInfo> callBack);

    @GET("/busstop/getStopLines")
    WaspRequest getStopLines(@Query("stopId") int stopId, BaseCallBack<GetStopLines> callBack);

    @GET("/buslines/getLineStops")
    WaspRequest getLineStop(@Query("lineId") int lineId, BaseCallBack<GetLineStops> callBack);

    //https://verisun.com:8543/ekentankara/rest/buslines/getLinePoints?lineId=125537
    @GET("/buslines/getLinePoints?lineId={lineId}")
    WaspRequest getLinePoints(@Path("lineId") int lineId, BaseCallBack<LinePoints> callBack);


    /***
     * BaseCallBack for Networking using
     * @param <T> T->Class Name
     */
    abstract class BaseCallBack<T> implements Callback<T> {
        @Override
        public void onSuccess(Response response, T t) {
            Log.e("RESPONSE=", "" + t);
            onSuccessResponse(t);
        }

        abstract public void onSuccessResponse(T t);

        abstract public void onError(String errorMessage);

        @Override
        public void onError(WaspError error) {
            Log.e("Error= ", error.toString());
            switch (error.getResponse().getStatusCode()) {
                case 0:
                    onError(BaseApp.instance.getString(R.string.err_timeout));
                    break;
                default:
                    onError(BaseApp.instance.getString(R.string.errorOccured));
                    break;
            }
        }
    }




}
