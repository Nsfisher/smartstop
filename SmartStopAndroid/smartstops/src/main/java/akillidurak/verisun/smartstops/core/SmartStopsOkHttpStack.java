package akillidurak.verisun.smartstops.core;

import android.content.Context;
import android.text.format.DateUtils;

import com.orhanobut.wasp.utils.SSLUtils;
import com.orhanobut.wasp.utils.WaspHttpStack;
import com.squareup.okhttp.ConnectionPool;
import com.squareup.okhttp.OkHttpClient;

import java.net.CookieHandler;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;

import akillidurak.verisun.smartstops.BuildConfig;

import static akillidurak.verisun.smartstops.core.App.context;

/**
 * Created by nsfisher on 25/01/17.
 */

public class SmartStopsOkHttpStack implements WaspHttpStack<OkHttpStack> {

    private final OkHttpStack okHttpStack;

    public SmartStopsOkHttpStack(Context context, int keyStoreRawResId, String keyStorePassword) {

        OkHttpClient okHttpClient = App.instance.getOkHttpClient().clone();
        okHttpClient.setConnectionPool(new ConnectionPool(2, DateUtils.MINUTE_IN_MILLIS));

        okHttpClient.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                HostnameVerifier hv =
                        HttpsURLConnection.getDefaultHostnameVerifier();
                return hostname.equals(BuildConfig.HOST_NAME) && hv.verify("verisun.com", session);
            }
        });
        okHttpClient.setSslSocketFactory(SSLUtils.getPinnedCertSslSocketFactory(context, keyStoreRawResId, keyStorePassword));
        this.okHttpStack = new OkHttpStack(okHttpClient);

    }


    @Override
    public OkHttpStack getHttpStack() {
        return this.okHttpStack;
    }

    @Override
    public void setHostnameVerifier(HostnameVerifier hostnameVerifier) {

    }

    @Override
    public void setSslSocketFactory(SSLSocketFactory sslSocketFactory) {

    }

    @Override
    public void setCookieHandler(CookieHandler cookieHandler) {

    }
}
