package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 */
public class Vehicles  {
    @SerializedName("bus")
    private int bus;

    @SerializedName("metro")
    private int metro;

    @SerializedName("tram")
    private int tramvay;

    @SerializedName("ferry")
    private int ferry;

    @SerializedName("ido")
    private int ido;

    @SerializedName("dentur")
    private int dentur;

    @SerializedName("turyol")
    private int turyol;

    @SerializedName("walk")
    private int walk;


    public int getBus() {
        return bus;
    }

    public void setBus(int bus) {
        this.bus = bus;
    }

    public int getMetro() {
        return metro;
    }

    public void setMetro(int metro) {
        this.metro = metro;
    }

    public int getTramvay() {
        return tramvay;
    }

    public void setTramvay(int tramvay) {
        this.tramvay = tramvay;
    }

    public int getFerry() {
        return ferry;
    }

    public void setFerry(int ferry) {
        this.ferry = ferry;
    }

    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }

    public int getDentur() {
        return dentur;
    }

    public void setDentur(int dentur) {
        this.dentur = dentur;
    }

    public int getTuryol() {
        return turyol;
    }

    public void setTuryol(int turyol) {
        this.turyol = turyol;
    }

    public int getWalk() {
        return walk;
    }

    public void setWalk(int walk) {
        this.walk = walk;
    }

    @Override
    public String toString() {
        return "vehicle {"+super.toString()    +  '\''               +
                "bus" + bus +  '\''               +
                "metro" + metro +  '\''               +
                "tram"  + tramvay    +  '\''               +
                "ferry"  + ferry    +  '\''               +
                "ido"  + ido    +  '\''               +
                "dentur"  + dentur    +  '\''               +
                "turyol"  + turyol    +  '\''               +
                "walk"  + walk    +  '\''               +  "}" ;
    }
}
