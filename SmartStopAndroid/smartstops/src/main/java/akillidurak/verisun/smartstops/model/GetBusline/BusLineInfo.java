package akillidurak.verisun.smartstops.model.GetBusline;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 14/02/17.
 */

public class BusLineInfo {

    /***
     * "busLineInfo":{
     "code":"203-3",
     "journeyDuration":70,
     "name":"İNCİRLİ-SOKULLU",
     "id":"125607",
     "stopCount":15,
     "lineDistance":48,
     "liveBuses":[]
     }
     */

    @SerializedName("code")
    private String lineId;

    @SerializedName("journeyDuration")
    private int journeyDuration;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    @SerializedName("stopCount")
    private int stopCount;

    @SerializedName("lineDistance")
    private int lineDistance;


    @SerializedName("liveBuses")
    private ArrayList<LiveBuses> liveBuses;

    public ArrayList<LiveBuses> getLiveBuses() {
        return liveBuses;
    }

    public int getJourneyDuration() {
        return journeyDuration;
    }

    public int getLineDistance() {
        return lineDistance;
    }

    public int getStopCount() {
        return stopCount;
    }

    public String getId() {
        return id;
    }

    public String getLineId() {
        return lineId;
    }

    public String getName() {
        return name;
    }
}
