package akillidurak.verisun.smartstops.model.VehicleLocation;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 25/05/16.
 */
public class NearestStop  {
    /** JSON Stop Mapping
    {
     "nearestStop":{
     "code":"402031",
     "name":"KADIKÖY",
     "directionDescription":"RIHTIM1 PERON",
     "timeToStop":5,
     "id":"70572"
     }
    }  **/

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("directionDescription")
    private String directionDescription ;

    @SerializedName("id")
    private String id;

    @SerializedName("timeToStop")
    private int timeToStop;









    /*
      @SerializedName("county")
    private String county;

    @SerializedName("latitude")
    private Double latitude;

    @SerializedName("longitude")
    private Double longitude;

      public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
     */

   //@SerializedName("id")
  // private  String ids;


    public String getId() {
        return id;
    }
    public int getTimeToStop() {
        return timeToStop;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public String getDirectionDescription() {
        return directionDescription;
    }

    @Override
    public String toString() {
        return "NearestStop {"
                +super.toString()     +  '\''  +
                "code"                + code       +  '\''  +
                "name"                + name       +  '\''  +
               // "Id"                  + ids         +  '\''  +
                "}"

                ;
    }
}
