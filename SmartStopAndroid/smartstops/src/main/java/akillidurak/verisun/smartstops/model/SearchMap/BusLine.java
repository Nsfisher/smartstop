package akillidurak.verisun.smartstops.model.SearchMap;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 * "busLines": [
 * {
 * "code": "B1",
 * "name": "BURÇ KAVŞAĞI-IBRAHIMLI-GAZIKENT",
 * "id": "231"
 * }
 * ]
 */
public class BusLine  {

    @SerializedName("code")
    private String code;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int ids;

    public int getIds() {
        return ids;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    @Override
    public String toString() {
        return "BusLine{" + '\'' +
                super.toString() + '\'' +
                "code" + code + '\'' +
                "name" + name + '\'' +
                "id" + ids + '\'' +
                '}';
    }
}

