package akillidurak.verisun.smartstops.model.TimeTable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by nsfisher on 20/12/16.
 */

public class TimeTableModel {

    /**
     * {
     * "processInfo":1,
     * "timeTable":{}
     * }
     */

    @SerializedName("processInfo")
    private int processInfo;
    @SerializedName("timeTable")
    private TimeTable timeTable;

    public int getProcessInfo() {
        return processInfo;
    }

    public TimeTable getTimeTable() {
        return timeTable;
    }


}
