package akillidurak.verisun.smartstops.model.VehicleLocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 25/05/16.
 *
 *      {
        "processInfo":1,
        "vehicleInfo":[
        {
            "vehicleType":"bus",
            "speed":32,
            "location":
            {
                "lat":"40.996971",
                "lon":"29.026369"
            },
            "distanceToPoint":"230",
            "lineInfo":
            {
            "lineCode":"E-11",
            "lineId":34765,
            "stops":
               [
                {
            "code":"402031",
            "county":"Kadıköy",
            "latitude":40.99295,
            "longitude":29.024071,
            "name":"KADIKÖY",
            "directionDescription":"RIHTIM1 PERON",
            "id":"70572"
               }
             ]
            }
        }
       ]
       }
 *
 */

public class VehicleLocationInfo  {

    @SerializedName("processInfo")
    private  String processInfo;

    @SerializedName("vehicleInfo")
    private ArrayList<VehicleInfo> vehicleInfos;

    public String getProcessInfo() {
        return processInfo;
    }

    public ArrayList<VehicleInfo> getVehicleInfos() {
        return vehicleInfos;
    }

    public void setProcessInfo(String processInfo) {
        this.processInfo = processInfo;
    }

    public void setVehicleInfos(ArrayList<VehicleInfo> vehicleInfos) {
        this.vehicleInfos = vehicleInfos;
    }

    //json response
    @Override
    public String toString() {
        return "VehicleLocationInfo{"         +  '\''  +
                super.toString()              +  '\''  +
                "processInfo" + processInfo   +  '\''  +
                "VehicleInfo" + vehicleInfos  +   '}';


    }
}
