package akillidurak.verisun.smartstops.model.SearchMap;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.


"busStops":[
 {
 "latitude": 37.0476,
 "longitude": 37.36861,
 "name": "KURBANBABA GENÇLIK MERKEZI",
 "id": "295"
 }
        ] */
public class BusStopsSearch  {


    @SerializedName("latitude")
    private Double latitude;

    @SerializedName("longitude")
    private Double longitude;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int ids;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getIds() {
        return ids;
    }



    @Override
    public String toString() {
        return "BusStopInfo{"               +  '\''               +
                super.toString()            +  '\''               +
                "latitude"                  + latitude            +  '\''  +
                "longitude"                 + longitude           +  '\''  +
                "name"                      + name                +  '\''  +
     //           "Id"                        + ids                 +  '\''  +
                '}';
    }


}
