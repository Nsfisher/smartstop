package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursVenues  {

  @SerializedName("venues")
  private ArrayList<FoursVenuesArray> venuesArrays;

  public ArrayList<FoursVenuesArray> getVenuesArrays() {
    return venuesArrays;
  }

  @Override
  public String toString() {
    return "Search{"                   +   '\''                +
            super.toString()           +  '\''                 +
            "venues"                      + venuesArrays           +  '\''  +
            '}';
  }

}
