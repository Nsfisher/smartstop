package akillidurak.verisun.smartstops.model.VehicleLocation;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 25/05/16.
 */
public class LineInfo  {

    /**{ JSON MAPPING
     "lineInfo":{
     "lineCode":"E-11",
     "lineId":34765,
     "nearestStop":{
     "code":"402031",
     "name":"KADIKÖY",
     "directionDescription":"RIHTIM1 PERON",
     "timeToStop":5,
     "id":"70572"
     }
     }
     }

     **/

    @SerializedName("lineCode")
    private String lineCode;

    @SerializedName("lineId")
    private int lineId;

    @SerializedName("nearestStop")
    private NearestStop  stopses;


    public String getLineCode() {
        return lineCode;
    }

    public void setLineCode(String lineCode) {
        this.lineCode = lineCode;
    }

    public int getLineId() {
        return lineId;
    }

    public void setLineId(int lineId) {
        this.lineId = lineId;
    }

    public NearestStop getStopses() {
        return stopses;
    }

    public void setStopses(NearestStop stopses) {
        this.stopses = stopses;
    }



    //json response
    @Override
    public String toString() {
        return "LineInfo{"            +  '\''  +
                super.toString()      +  '\''  +
                "lineCode"            + lineCode     +  '\''  +
                "stops"               + stopses      +  '\''  +
                '}';
    }
}
