package akillidurak.verisun.smartstops.model.TimeTable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 20/12/16.
 */

public class LineSchecule  {

    /***
     lineSchedules": [
     "lineSchedules": [
     {
     "WORKDAY": [
     "06:00",
     "06:20",
     "06:40",
     "07:00",
     "07:22",
     "07:38"
     ]
     },
     {
     "SATURDAY": [
     "06:00",
     "06:20",
     "06:40",
     "07:00"
     ]
     },
     {
     "SUNDAY": [
     "06:00",
     "06:20",
     "06:40",
     "07:00",
     "07:22",
     "07:38"
     ]
     }


    @SerializedName("dayType")
    private String dayType;
    @SerializedName("time")
    private String time;






    public String getDayType() {
        return dayType;
    }

    public String getTime() {
        return time;
    }

*/

    @SerializedName("WORKDAY")
    private ArrayList<String> workdayList;

    @SerializedName("SATURDAY")
    private ArrayList<String> saturdayList;

    @SerializedName("SUNDAY")
    private ArrayList<String> sundayList;

    public ArrayList<String> getSaturdayList() {
        return saturdayList;
    }

    public ArrayList<String> getSundayList() {
        return sundayList;
    }

    public ArrayList<String> getWorkdayList() {
        return workdayList;
    }
}
