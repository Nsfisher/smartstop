package akillidurak.verisun.smartstops.model.LineStops;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by nsfisher on 16/02/17.
 */

public class GetLineStops {

    /***
     * {
     "processInfo":1,
     "lineStops":[]
     }
     */
    @SerializedName("processInfo")
    private String processInfo;
    @SerializedName("lineStops")
    private ArrayList<LineStops> lineStopses;

    public ArrayList<LineStops> getLineStopses() {
        return lineStopses;
    }

    public String getProcessInfo() {
        return processInfo;
    }


}
