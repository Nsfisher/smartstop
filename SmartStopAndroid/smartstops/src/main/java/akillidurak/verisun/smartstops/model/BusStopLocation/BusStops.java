package akillidurak.verisun.smartstops.model.BusStopLocation;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;
import akillidurak.verisun.smartstops.model.BaseModel;


/**
 * Created by nsfisher on 26/05/16.
 */
 /**
"busStops":[
        {
        "code":"402221",
        "county":"Kadıköy",
        "location":{
        "latitude":40.998787,
        "longitude":29.056355
        },
        "name":"UZUNÇAYIR METROBÜS",
        "type":"MODERN",
        "id":"70588",
        "distanceToPoint":43

        },
        {
        "code":"402221",
        "county":"Kadıköy",
        "location":{
        "latitude":40.998787,
        "longitude":29.056355
        },
        "name":"UZUNÇAYIR E-5",
        "type":"MODERN",
        "id":"70588",
        "distanceToPoint":234
        }
        ]
  **/
public class BusStops {
     /*
     @SerializedName("code")
     private String code;

          public String getCode() {
         return code;
     }

     public void setCode(String code) {
         this.code = code;
     }

          @SerializedName("type")
     private String type;

     public String getType() {
         return type;
     }

     public void setType(String type) {
         this.type = type;
     }   @SerializedName("distanceToPoint")
     private  int distanceToPoint;

          public int getDistanceToPoint() {
         return distanceToPoint;
     }

     public void setDistanceToPoint(int distanceToPoint) {
         this.distanceToPoint = distanceToPoint;
     }
     @SerializedName("county")
     private String county;

    */


     @SerializedName("location")
     private Locations locations;

     @SerializedName("name")
     private String name;

     @SerializedName("busStopId")
     private String ids;


     public Locations getLocations() {
         return locations;
     }

     public void setLocations(Locations locations) {
         this.locations = locations;
     }

     public String getIds() {
         return ids;
     }

     public void setIds(String ids) {
         this.ids = ids;
     }

     public String getName() {
         return name;
     }

     public void setName(String name) {
         this.name = name;
     }


     @Override

     public String toString() {
         return "BusStopsSearch{"               +  '\''               +
                 super.toString()         +  '\''               +
                 "location"               + locations           +  '\''  +
                 "name"                   + name                +  '\''  +
                    "id"                     + ids                 +  '\''  +

                 '}';
     }
 }
