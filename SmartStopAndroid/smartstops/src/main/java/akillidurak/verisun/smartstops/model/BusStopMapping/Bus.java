package akillidurak.verisun.smartstops.model.BusStopMapping;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;
import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;

/**
 * Created by nsfisher on 26/05/16.
 */
public class Bus {
 /**
  * JSON RESPONSE
  "buses": [
  {
  "lineId": "125501",
  "code": "182",
  "name": "HAKİMİYET YENİŞEHİR İSTASYON ",
  "timeToStop": 1,
  "distanceToStop": 3.0296160370503378,
  "nextStopId": 9627,
  "nextStopName": "KAFKASLAR 1. DURAK",
  "previousStopId": 7703,
  "previousStopName": "OTOBAN YOL AYRIMI",
  "plate": "06 EA 666"
  },
  {
  "lineId": "125175",
  "code": "188",
  "name": "SÜMEREVLER OYMAK CADDESİ",
  "timeToStop": 2,
  "distanceToStop": 2.839404915637987,
  "nextStopId": 1949,
  "nextStopName": "KAFKASLAR 1. DURAK",
  "previousStopId": 5307,
  "previousStopName": "OTOBAN YOL AYRIMI",
  "plate": "06 EA 666"
  }
  ],
  **/
 @SerializedName("lineId")
 private String lineId;

 @SerializedName("busCode")
 private String code;

 @SerializedName("busName")
 private String name;

 @SerializedName("timeToStop")
 private int timeToStop;

 @SerializedName("distanceToStop")
 private double distanceToStop;

 @SerializedName("nextStopId")
 private int nextStopId;
 @SerializedName("nextStopName")
 private String nextStopName;


 @SerializedName("previousStopId")
 private int previousStopId;

 @SerializedName("previousStopName")
 private String previousStopName;


 @SerializedName("plate")
 private String plate;

 @SerializedName("busLocation")
 private Locations locations;


 public Locations getLocations() {
  return locations;
 }

 public String getLineId() {
  return lineId;
 }

 public String getCode() {
  return code;
 }
 public String getName() {
  return name;
 }
 public int getTimeToStop() {
  return timeToStop;
 }

 public String getNextStopName() {
  return nextStopName;
 }

 public String getPreviousStopName() {
  return previousStopName;
 }

 public double getDistanceToStop() {
  return distanceToStop;
 }

 public double getNextStopId() {
  return nextStopId;
 }

 public double getPreviousStopId() {
  return previousStopId;
 }

 public String getPlate() {
  return plate;
 }

 public void setPreviousStopId(int previousStopId) {
  this.previousStopId = previousStopId;
 }

 @Override
    public String toString() {
        return "Bus{"                       +  '\''               +
                super.toString()            +  '\''               +
                "lineId"                    + lineId            +  '\''  +
                "code"                      + code                +  '\''  +
                "name"                      + name                +  '\''  +
                "timeToStop"                + timeToStop          +  '\''  +
                "distanceToStop"            + distanceToStop      +  '\''  +
                "nextStopId"                + nextStopId          +  '\''  +
                "previousStopId"            + previousStopId      +  '\''  +
                "plate"                     + plate               +  '\''  +

                '}';
    }




}
