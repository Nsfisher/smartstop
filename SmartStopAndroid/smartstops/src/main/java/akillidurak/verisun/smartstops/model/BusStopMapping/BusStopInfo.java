package akillidurak.verisun.smartstops.model.BusStopMapping;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;
import akillidurak.verisun.smartstops.model.VehicleLocation.Locations;

/**
 * Created by nsfisher on 26/05/16.
 */
  /**  JSON RESPONSE
    {
        "processInfo":1,
        "stopType":"BUS",
        "busCode":"402221",
        "county":"Kadıköy",
        "location":{
        "latitude":40.998787,
        "longitude":29.056355
        },
        "name":"UZUNÇAYIR METROBÜS",
        "buses":[

        {
        "code":"B43",
        "journeyDuration":100,
        "name":"IPEKEVLER-SEFAŞEHIR",
        "type":"1",
        "directions":[
        "DEPARTURE"
        ],
        "id":"112",
        "timeToStop":15,
        "stopCount":4,
        "distanceToStop":3.3963895870775325
        },
        {
        "code":"B43",
        "journeyDuration":100,
        "name":"SEFAŞEHIR-IPEKEVLER",
        "type":"1",
        "directions":[
        "DEPARTURE"
        ],
        "id":"273",
        "timeToStop":19,
        "stopCount":1,
        "distanceToStop":0
        }
        ]
        }

        **/
public class BusStopInfo  {

    @SerializedName("processInfo")
    private int processInfo;



    @SerializedName("code")
    private String code;

    @SerializedName("location")
    private Locations locations;

    @SerializedName("name")
    private String name;

    @SerializedName("buses")
    private ArrayList<Bus> bus;


      /*
    @SerializedName("stopType")
    private String stopType;
    public String getStopType() {
        return stopType;
    }
    public void setStopType(String stopType) {
        this.stopType = stopType;
    }


     @SerializedName("county")
    private String county;

       */

      public int getProcessInfo() {
        return processInfo;
    }

    public void setProcessInfo(int processInfo) {
        this.processInfo = processInfo;
    }



    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }





    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

      public ArrayList<Bus> getBus() {
          return bus;
      }

      public void setBus(ArrayList<Bus> bus) {
          this.bus = bus;
      }

      @Override
    public String toString() {
        return "BusStopInfo{"               +  '\''               +
                super.toString()            +  '\''               +
         //       "processInfo"               + processInfo         +  '\''  +
                "code"                      + code                +  '\''  +
                "name"                      + name                +  '\''  +
                "location"                  + locations           +  '\''  +
                "name"                      + name                +  '\''  +
                "buses"                     + bus                 +  '\''  +
                '}';
    }



}
