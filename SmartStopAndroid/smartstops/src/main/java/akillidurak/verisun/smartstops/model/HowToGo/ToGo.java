package akillidurak.verisun.smartstops.model.HowToGo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 26/05/16.
 */
public class ToGo  {




    @SerializedName("stats")
    private Stats stats;

    @SerializedName("vehicles")
    private Vehicles vehicles;

    @SerializedName("path")
    private ArrayList<Path> paths;

    @SerializedName("count")
    private int count;

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }

    public ArrayList<Path> getPaths() {
        return paths;
    }

    public void setPaths(ArrayList<Path> paths) {
        this.paths = paths;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Vehicles getVehicles() {
        return vehicles;
    }

    public void setVehicles(Vehicles vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public String toString() {
        return "How2Go{"                   +   '\''                +
                super.toString()           +  '\''                 +
                "stats"              + stats           +  '\''  +
                "vehicle"                 + vehicles               +  '\''  +
   //             "path"                 + paths        +  '\''  +
                "count"                   + count                +  '\''  +
                '}';
    }
}
