package akillidurak.verisun.smartstops.model.Foursquare;

import com.google.gson.annotations.SerializedName;

import akillidurak.verisun.smartstops.model.BaseModel;

/**
 * Created by nsfisher on 29/06/16.
 */
public class FoursContact   {

    @SerializedName("phone")
    private String phone;

    @SerializedName("formattedPhone")
    private String formattedPhone;

    @SerializedName("twitter")
    private String twitter;
}
