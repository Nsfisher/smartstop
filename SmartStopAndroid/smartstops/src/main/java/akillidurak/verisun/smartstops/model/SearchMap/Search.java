package akillidurak.verisun.smartstops.model.SearchMap;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import akillidurak.verisun.smartstops.model.BaseModel;
import akillidurak.verisun.smartstops.model.TimeTable.Adress;

/**
 * Created by nsfisher on 26/05/16.
 */

  /**
   {
   "busLines":[],
   "adress":[],
   "busStops":[],
   "processInfo":1
   }
        }*/
public class Search {

      @SerializedName("processInfo")
      private String processInfo ;

      @SerializedName("busLines")
      private ArrayList<BusLine> busLine;

      @SerializedName("busStops")
      private ArrayList<BusStopsSearch> busStopsSearch;

      @SerializedName("adress")
      private ArrayList<Adress> adress;

      public String getProcessInfo() {
          return processInfo;
      }

      public ArrayList<BusLine> getBusLine() {
          return busLine;
      }
      public ArrayList<BusStopsSearch> getBusStopsSearch() {
          return busStopsSearch;
      }

      public ArrayList<Adress> getAdress() {
          return adress;
      }

      @Override
      public String toString() {
          return "Search{"                   +   '\''                +
                  super.toString()           +  '\''                 +
                  "processInfo"              + processInfo           +  '\''  +
                  "busLines"                 + busLine               +  '\''  +
                  "busStops"                 + busStopsSearch        +  '\''  +
                  "adress"                   + adress                +  '\''  +
                  '}';
      }
  }
